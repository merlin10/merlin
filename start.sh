#venv
virtualenv /home/ubuntu/venv39
source /home/ubuntu/venv39/bin/activate

#uvicorn (level down from FASTAPI, mediates socket and port)
uvicorn merlin.main:app
uvicorn --host 0.0.0.0 merlin.main:app
potom ip a a zjistuju IP adresu wlp (zalezi, pres co jsem pripojena)

#docker on backround (for running postgres db)
#https://docs.docker.com/engine/install/ubuntu/
sudo docker run --name postgres-db -d -e POSTGRES_PASSWORD=password postgres
sudo docker exec -it postgres-db bash
psql -U postgres

#asyncpg (for connecting to db)
#https://magicstack.github.io/asyncpg/current/usage.html
pip install asyncpg

    #connection string to db (db name postgres, password password, IP dockeru cmd ip a)
    conn = await asyncpg.connect('postgresql://postgres:password@172.17.0.2/postgres')
    await conn.execute('''
        INSERT INTO photo_metadata(file_type,wgs_lat,wgs_lot,date_created,date_uploaded, description, codec)
        VALUES($1, $2, $3, $4, $5, $6, $7)''', 'photo', 2.3, 1.2, datetime.date(2021,7,13),None,None,None
    )

# pripojeni k DB
psql -d merlin -U merlin-user -h localhost