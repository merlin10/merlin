import asyncio
from aio_pika import connect, IncomingMessage
from wand.image import Image
from merlin.context import GlobalContext
import json

async def photo_resize(photo_id: int):
    resp = await GlobalContext.object_store.get_object(
        Bucket="merlin-photos", Key=str(photo_id)
    )
    img = await resp["Body"].read()
    content_type = resp["ContentType"]
    # small_img = img.resize(300)
    # print(small_img.width, small_img.height)
    with Image(blob=img) as img:
        #img.transform(resize='x100')
        width, height = img.size
        new_height = round(300 * height / width)
        img.resize(300, new_height)
        small_img = img.make_blob()

    small_photo = await GlobalContext.object_store.put_object(
        Bucket="merlin-photos",
        Key=(f"{photo_id}_small"),
        Body=small_img,
        ContentType=content_type,
    )

async def on_message(message: IncomingMessage):
    """
    on_message doesn't necessarily have to be defined as async.
    Here it is to show that it's possible.
    """
    print(" [x] Received message %r" % message)
    print("Message body is: %r" % message.body) #precist body, parsnout json a misto uspani zavolat resize funkci s id ze zpravy
    body = message.body
    body_parsed = json.loads(body)
    photo_id = body_parsed["id"]
    await photo_resize(photo_id)
    #print("Before sleep!")
    #await asyncio.sleep(5)  # Represents async I/O operations
    #print("After sleep!")


async def main():
    await GlobalContext.init()

    try:
        # Creating a channel
        channel = await GlobalContext.rabbit_queue.channel()

        # Declaring queue
        queue = await channel.declare_queue("photo_upload")

        # Start listening the queue with name 'hello'
        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    await on_message(message)

    finally:
        await GlobalContext.close()


if __name__ == "__main__":
    # we enter a never-ending loop that waits for data and
    # runs callbacks whenever necessary.
    print(" [*] Waiting for messages. To exit press CTRL+C")
    asyncio.run(main())



