import aiobotocore
import asyncpg
import asyncio
from aio_pika import connect, Message

MINIO_ACCESS_KEY_ID = "test-user"
MINIO_SECRET_ACCESS_KEY = "test-password"
MINIO_ENDPOINT = "http://localhost:9000"

DB_URL='postgresql://merlin-user:merlin-pass@localhost/merlin'

RABBIT_MQ='amqp://guest:guest@localhost/'

class GlobalContext:                                        # do tridy obalime metody a 'parametry', ktere chci mit v celem merlinovi jen jednou
    object_store = None                                     # nechci aby se mi tvorilo spojeni do db atd pokazde, bylo by to sice ok a fungovalo by to, ale bylo by to mnohem pomalejsi
    db_pool: asyncpg.Pool = None                            # tady mi do init funkce pod parametrem cls vstupuje GlobalContext trida (object_store,db_pool,rabbit_queue parametry) a modifikuji se classmethodami
    rabbit_queue: None                                      # to jsou staticke metody, dynamicke totiz operuji nad objekty, staticke nad tou tridou, protoze objekty jsou tvorene dynamicky a trida je staticka
                                                            # muzu potom tedy z jinych skriptu zavolat GlobalContext.object_store a mam spojeni
    @classmethod                                            # normalne pouzivam tridu k vytvoreni objektu, class Nazev(a tady mam vstupujici parametry): dale mam 'specialni metodu' tridy - konstruktor - inicializuje tvorbu objektu, ktere ty parametry nejak modifikuji a vraci se objekt
    async def init(cls):                                    # Stepan me uz prudi ze musim jit behat, metoda je funkce, ktera operuje nad objektem, jeji prvni parametr/argument je objekt (self)
        session = aiobotocore.get_session()
        #cls.object_store = await session._create_client(
        #    's3',
        #    endpoint_url=MINIO_ENDPOINT,
        #    aws_secret_access_key=MINIO_SECRET_ACCESS_KEY,
        #    aws_access_key_id=MINIO_ACCESS_KEY_ID,
        #)
        cls.object_store = await session.create_client(
            's3',
            endpoint_url=MINIO_ENDPOINT,
            aws_secret_access_key=MINIO_SECRET_ACCESS_KEY,
            aws_access_key_id=MINIO_ACCESS_KEY_ID,
        ).__aenter__()
        cls.db_pool = await asyncpg.create_pool(DB_URL)
        cls.rabbit_queue = await connect(RABBIT_MQ)

    @classmethod
    async def close(cls):
        if cls.object_store is not None:
            await cls.object_store.__aexit__(None, None, None)
        if cls.db_pool is not None:
            await cls.db_pool.close()
        if cls.rabbit_queue is not None:
            await cls.rabbit_queue.close()