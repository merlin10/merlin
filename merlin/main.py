import io
import json
from enum import Enum
from asyncpg import connection
from datetime import datetime, date
import botocore

from fastapi import FastAPI, Header, HTTPException, Query
from fastapi.param_functions import File
from fastapi.requests import Request
from pydantic import BaseModel
import exifread
import pydantic
import asyncpg
from merlin.context import GlobalContext
from typing import List
from fastapi.responses import Response, StreamingResponse, FileResponse, HTMLResponse
from botocore.errorfactory import BaseClientExceptions

import asyncio
from aio_pika import connect, Message


app = FastAPI()


@app.on_event("startup")
async def startup_event():
    await GlobalContext.init()


@app.on_event("shutdown")
async def shutdown_event():
    await GlobalContext.close()


class FileType(str, Enum):
    photo = "photo"
    document = "document"


class Input_PhotoMetadata(BaseModel):
    type: FileType
    description: str


class SortDirection(str, Enum):
    asc = "asc"
    desc = "desc"


class PhotoSize(str, Enum):
    small = "small"
    original = "original"


class Output_PhotoData(BaseModel):
    id: int
    date_created: datetime = None
    description: str = None


class Non_JSON_response(BaseModel):
    id: str


# {"type":"photo","description":"Vnitroblock"}


@app.get("/")  # router
async def index():
    return FileResponse("merlin/frontend_photos.html")


@app.get("/js")  # router
async def get_filter():
    return FileResponse("merlin/front_and_back.js")

@app.get("/css")  # router
async def get_filter():
    return FileResponse("merlin/design.css")


@app.post("/photo")  # router
async def photo_upload(  # handler
    request: Request, x_photo_metadata: str = Header(...)
):
    try:
        metadata = Input_PhotoMetadata.parse_raw(x_photo_metadata)
    except pydantic.ValidationError as exc:
        raise HTTPException(400, str(exc))

    # Store metadata in SQL
    time_now = datetime.now()
    row = await GlobalContext.db_pool.fetchrow(
        """
        INSERT INTO photo_metadata(
            file_type,
            wgs_lat,
            wgs_lon,
            date_created,
            date_uploaded,
            description,
            codec
        )
        VALUES($1, $2, $3, $4, $5, $6, $7) RETURNING id""",
        metadata.type,
        None,
        None,
        None,
        time_now,
        metadata.description,
        None,
    )

    photo_id = row["id"]
    # Save file to object store
    body = await request.body()
    content_type = request.headers["content-type"]
    resp = await GlobalContext.object_store.put_object(
        Bucket="merlin-photos", Key=str(photo_id), Body=body, ContentType=content_type
    )
    msg = json.dumps({"id":photo_id})
    photo_upload_channel = await GlobalContext.rabbit_queue.channel()
    await photo_upload_channel.default_exchange.publish(
        Message(msg.encode()),
        routing_key='photo_upload'
    )
    print(f' [x] Sent Photo ID:{photo_id}')
    # with open(f'fotky/{photo_id}', "wb+") as f:
    #    f.write(body)

    # Process file exif data
    body_reader = io.BytesIO(body)
    exif_tags = exifread.process_file(body_reader, details=False)
    wgs_lat = None
    wgs_lon = None
    created_date = time_now
    if "GPS GPSLatitude" in exif_tags and "GPS GPSLongitude" in exif_tags:
        exif_lat = exif_tags["GPS GPSLatitude"].values
        wgs_lat = (
            exif_lat[0].decimal()
            + exif_lat[1].decimal() / 60
            + exif_lat[2].decimal() / 3600
        )
        exif_lon = exif_tags["GPS GPSLongitude"].values
        wgs_lon = (
            exif_lon[0].decimal()
            + exif_lon[1].decimal() / 60
            + exif_lon[2].decimal() / 3600
        )
        print(f"PHOTO GPS: {wgs_lat} {wgs_lon}")
    if "Image DateTime" in exif_tags:
        created_date = datetime.strptime(
            exif_tags["Image DateTime"].values, "%Y:%m:%d %H:%M:%S"
        )

    row = await GlobalContext.db_pool.execute(
        """
        UPDATE photo_metadata
        SET wgs_lat = $1,
            wgs_lon = $2,
            date_created = $3,
            codec = $4
        WHERE id = $5;
        """,
        wgs_lat,
        wgs_lon,
        created_date,
        None,
        photo_id,
    )
    return ""



@app.get("/photo/search",
    response_model=List[Output_PhotoData],
    response_model_exclude_unset=True,
)
async def photo_search(
    date_created_from: date = Query(None),
    date_created_to: date = Query(None),
    date_created_sort: SortDirection = Query(SortDirection.desc),
    file_type: FileType = Query(None),
):

    filters = []
    args = []
    arg_cnt = 1
    if date_created_from is not None:
        filters.append(f"date_created >= ${arg_cnt}")
        arg_cnt += 1
        args.append(date_created_from)
    if date_created_to is not None:
        filters.append(f"date_created <= ${arg_cnt}")
        arg_cnt += 1
        args.append(date_created_to)
    if file_type is not None:
        filters.append(f"file_type = ${arg_cnt}")
        arg_cnt += 1
        args.append(file_type)

    filters_q = " AND ".join(filters)
    if filters != []:
        filters_q = "WHERE " + filters_q

    q = (
        "SELECT id, date_created, description FROM photo_metadata "
        + filters_q
        + f" ORDER BY date_created {date_created_sort}"
    )

    rows = await GlobalContext.db_pool.fetch(q, *args)
    print(f"\n\nROWS = {rows}")
    print(q)
    print(args)

    # return [dict(row) for row in rows]
    return rows


@app.get("/photo/{photo_id}")
async def photo_get(photo_id: int, size: PhotoSize = Query(PhotoSize.original)):
    if size == PhotoSize.small:
        key = f'{photo_id}_small'
    else: 
        key = str(photo_id)
    try:
        resp = await GlobalContext.object_store.get_object(
            Bucket="merlin-photos", Key=key
        )
    except Exception as e:
        return Response(content=repr(e), status_code=400)
    stream = resp["Body"]
    content_type = resp["ContentType"]
    return StreamingResponse(stream.iter_chunks(), media_type=content_type)


@app.get("/documents")
async def index():
    return FileResponse("merlin/frontend_documents.html")

@app.get("/document/{document_id}/details")
async def index(document_id: int):
    content = f"""
<!DOCTYPE html>
<html>

<head>
    <title>Merlin - document details</title>
    <script src="/js"></script>
    <link rel="stylesheet" type="text/css" href="/css" />
</head>

<body>
    <img src="/photo/{document_id}?size=small" id='image_bill' width="500" height="600">
    <h1>Add details</h1>
    <p></p>
    <label for="vendor">Vendor:</label>
    <input type="text" id="vendor" name="vendor">
    <p></p>
    <label for="shopping_date">Shopping date:</label>
    <input type="date" id="shopping_date" name="shopping_date">
    <p></p>
    <label for="value">Value:</label>
    <input type="number" id="value" name="value">
    <p></p>
    <label for="items">Items:</label>
    <input type="text" id="items" name="items">
    <br>
    <br>
    <button onclick="add_doc_details()">Submit</button>
    <div class="row" id="bill_container">
        <div class="column" id="column">

        </div>
</body>
</html>
"""
    return HTMLResponse(content=content)


@app.get("/documents_search")
async def all_documents():
    q = (
        "SELECT * FROM photo_metadata WHERE file_type = 'document';"
    )
    rows = await GlobalContext.db_pool.fetch(q)
    print(rows)
    return rows


'''@app.get("/document/{document_id}")
async def document_get(document_id: int):
    key = str(document_id)
    resp = await GlobalContext.object_store.get_object(
        Bucket="merlin-photos", Key=key
    )
    stream = resp["Body"]
    content_type = resp["ContentType"]
    return StreamingResponse(stream.iter_chunks(), media_type=content_type)'''