function formatParams(params) {
    return "?" + Object
        .keys(params)
        .map(function(key) {
            return key + "=" + encodeURIComponent(params[key])
        })
        .join("&")
}

function get_filters() {
    var inputs = {}
    inputs["date_created_from"] = document.getElementById("date_created_from").value
    inputs["date_created_to"] = document.getElementById("date_created_to").value
    inputs["file_type"] = document.getElementById("file_type").value
    inputs["date_created_sort"] = document.getElementById("date_created_sort").value

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState != 4) {
            return
        }

        if (this.status != 200) {
            console.log("Error: Status is different from 200 " + xhttp.responseText)
            return
        }

        if (xhttp.responseText == '') {
            console.log("Error: Empty input")
            return
        }

        var parsed_resp = JSON.parse(xhttp.responseText);

        console.log(parsed_resp)
        console.log(xhttp.responseText)
        var photo_container1 = document.getElementById("column1")
        var photo_container2 = document.getElementById("column2")
        var photo_container3 = document.getElementById("column3")
        var photo_container4 = document.getElementById("column4")
        var HTML_text_col1 = ''
        var HTML_text_col2 = ''
        var HTML_text_col3 = ''
        var HTML_text_col4 = ''

        for (let i = 0; i < parsed_resp.length; i++) {
            let photo_item = parsed_resp[i]
                //img_tag = "<img src=\"/photo/" + photo_item.id + "?size=small\">"
                //img_tag = "<a href=\"/photo/" + photo_item.id + "\"><img src=\"/photo/" + photo_item.id + "?size=small\"></a>\""
            img_tag = `<a href=\"/photo/${photo_item.id}\"><img src=\"/photo/${photo_item.id}?size=small\"></a>`
            let mod = i % 4
            if (mod == 0) {
                HTML_text_col1 += img_tag
            } else if (mod == 1) {
                HTML_text_col2 += img_tag
            } else if (mod == 2) {
                HTML_text_col3 += img_tag
            } else if (mod == 3) {
                HTML_text_col4 += img_tag
            }
        }


        photo_container1.innerHTML = HTML_text_col1
        photo_container2.innerHTML = HTML_text_col2
        photo_container3.innerHTML = HTML_text_col3
        photo_container4.innerHTML = HTML_text_col4
    }

    var url = "/photo/search" + formatParams(inputs);
    xhttp.open("GET", url, true);
    xhttp.send();
};


function show_bill() {
    console.log("test")
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState != 4) {
            return
        }

        if (this.status != 200) {
            console.log("Error: Status is different from 200 " + xhttp.responseText)
            return
        }

        if (xhttp.responseText == '') {
            console.log("Error: Empty input")
            return
        }

        var parsed_resp = JSON.parse(xhttp.responseText);
        var bill_container = document.getElementById("column")
        var HTML_text_col = ''


        for (let i = 0; i < parsed_resp.length; i++) {
            let bill_item = parsed_resp[i]
                //bill_tag = `<p><button onclick="get_filters()">BILL ID:${bill_item.id}</button></p>`
            bill_tag = `<p><a href="/document/${bill_item.id}/details">BILL ID:${bill_item.id}</a></p>`

            HTML_text_col += bill_tag
        }

        bill_container.innerHTML = HTML_text_col
    }
    window.onload = function() {
        document.getElementById('image_bill').src = `/photo/${bill_item.id}`
    }
    var url = "/documents_search";
    xhttp.open("GET", url, true);
    xhttp.send();
}

function add_doc_details() {
    var inputs = {}
    inputs["vendor"] = document.getElementById("vendor").value
    inputs["shopping_date"] = document.getElementById("shopping_date").value
    inputs["value"] = document.getElementById("value").value
    inputs["items"] = document.getElementById("items").value

};