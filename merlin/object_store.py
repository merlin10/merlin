# NEPOUZIVAME, TEST VERZE

import asyncio
import aiobotocore

AWS_ACCESS_KEY_ID = "test-user"
AWS_SECRET_ACCESS_KEY = "test-password"


async def go():
    bucket = "merlin-photos"
    key = "test/testfile2"

    session = aiobotocore.get_session()
    async with session.create_client(
        's3',
        endpoint_url='http://172.20.10.6:9000',
        aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
        aws_access_key_id=AWS_ACCESS_KEY_ID,
    ) as client:
        # upload object to amazon s3
        data = b"\x01" * 1024
        resp = await client.put_object(Bucket=bucket, Key=key, Body=data)
asyncio.run(go())
        