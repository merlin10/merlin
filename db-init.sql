CREATE TABLE photo_metadata(
    id SERIAL PRIMARY KEY,
    file_type VARCHAR NOT NULL,
    wgs_lat FLOAT,
    wgs_lon FLOAT,
    date_created TIMESTAMP,
    date_uploaded TIMESTAMP,
    description VARCHAR,
    codec VARCHAR
);

CREATE TABLE document_bill(
    id SERIAL PRIMARY KEY,
    vendor VARCHAR,
    items VARCHAR,
    date_created TIMESTAMP,
    date_uploaded TIMESTAMP,
    value FLOAT
);